import random
import numpy as np

usedAlpha = []

class letter():
    l = ""
    wrongPos = False
    rightPos = False

    def __init__(self, lt, wP, rP):
        self.l = lt
        self.wrongPos = wP
        self.rightPos = rP



def getGuesses():
    guess = int(input("How many guesses would you like: "))
    return guess

def getWord():
    wordList = ["Hello", "Goods", "Zebra"]

    com = input("To have a random word, type \"Random\". Otherwise, type in a word to use. ").lower()

    if(com == "random"):
        com = wordList[random.randint(0, len(wordList) - 1)]

    return com

def setupGUI(answer):
    #bottom = np.full((1, len(answer)), "-")
    #top = np.full((1, len(answer)), "X")
    #arr = np.stack((top, bottom), axis=0)

    arr = np.array([["X", "X", "X", "X", "X"],
                    ["-", "-", "-", "-", "-"]
                    ])

    return arr

def editGUI(gui, letterList, wordGuess):
    for i in letterList:
        if(i.wrongPos == True):
            gui[0, letterList.index(i)] = "Y"
            gui[1, letterList.index(i)] = str(wordGuess[wordGuess.index(i.l)])
        elif(i.rightPos == True):
            gui[0, letterList.index(i)] = "G"
            gui[1, letterList.index(i)] = str(wordGuess[wordGuess.index(i.l)])


def setupLetterList(answer):
    answerObjList = []
    for i in answer:
        obj = letter(i.lower(), False, False)
        answerObjList.append(obj)
    return answerObjList

def main():
    print("Welcome to Bleecker's Wordle!\n")
    guesses = getGuesses()
    answer = getWord()
    gui = setupGUI(answer)
    letterList = setupLetterList(answer)

    guessedWords = []
    guessedLetters = []
    commandList = ["quit", "help", "alpha", "guessed words"]
    noWin = True

    while guesses > 0 and noWin:

        print("The current situation is")
        print(gui)
        print("\n\n")

        g = input("Guess a word or type \"help\" for a list of options: ").lower()

        if(g not in commandList):
            wordGuess = list(g)

            for i in wordGuess:
                for j in letterList:
                    if(i == j.l and wordGuess.index(i) == letterList.index(j)):
                        j.rightPos = True
                        j.wrongPos = False
                    elif(i == j.l and not wordGuess.index(i) == letterList.index(j)):
                        j.wrongPos = True
                        j.rightPos = False
        
            for k in wordGuess:
                guessedLetters.append(k)

            guessedWords.append("".join(wordGuess))


            yesCount = 0
            for l in letterList:
                if(l.rightPos == True):
                    yesCount += 1
            
            if(yesCount == len(wordGuess)):
                noWin = False
                cont = input("CONGRATS! YOU WIN!!! Would you like to play again? ").lower()
                if(cont == "yes"):
                    main()
                else:
                    pass
            
            else:
                print(editGUI(gui, letterList, wordGuess))

                guesses -= 1
                print("\n\nYou have " + str(guesses) + " guesses left.")

        else:
            if(g == "help"):
                print("Coming soon!")
            elif(g == "alpha"):
                print(guessedLetters)
            elif(g == "guessed words"):
                print(guessedWords)

    
    
main()




