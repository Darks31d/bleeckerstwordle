# BleeckerStWordle

My own version of wordle that adds in and changes a few things from the original, such as:

- Support for words where len > 5.
- You determine the number of guesses
- The ability to get hints at the cost of guesses
And more.


If you want, make edits to this. Happy to have them.
